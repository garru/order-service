<br />
<p  style="background: #1351AA; text-align: center">
  <a href="https://gitlab.com/garru/order-service.git">
    <img src="./src/assets/logo.png" alt="Logo" width="400">
  </a>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open" style="">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li>
    <a href="#usage">Usage</a>
     <ul>
        <li><a href="#adding-new-order">Add new order</a></li>
        <li><a href="#web-structure">Web structure</a></li>
        <li><a href="#main-features">Main features</a></li>
        <li><a href="#extra-features">Extra features</a></li>
      </ul>
    </li> 
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
An test use for preview, created by BAEMIN and finish by Do Huynh The Dan.
Create an order tracking service.


### Built With

Frontend:
* [React](https://reactjs.org/)
* [Typescript](https://www.typescriptlang.org/)
* [Webpack](https://webpack.js.org/)
* [Redux](https://redux.js.org/)

Backend:
* No backend, I used localStorage as a DB


<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/garru/order-service.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Start app
   ```sh
   npm start
   ```


<!-- USAGE EXAMPLES -->
## Usage
### Adding new order
1. Adding new order:

    Order will be created by another system, so we can't create new order on this app.
    However, we can add new order by insert new Object with interface in to file "src/shared/dummyData.tsx"
    ```sh
        Order = {
            id: number;
            customerName: string;
            riderName: string;
            orderStatus: string;
            orderAddress: string;
            merchantName: string;
            merchantAddress: string;
            dishes: Dish[];
            totalPrice: number;
            updatedTime: string;
            processingStatus?: string;
            assignedTo?: string;
        };
    ```
### Web structure
2. Web structure:

    <img src="./src/assets/demo1.png" alt="Logo" width="400">
    
    We have 3 main modules:
    * Order Management System: Default view of Operators
    
    <img src="./src/assets/demo2.png" alt="Logo" width="400">

    * Order Management System With User Role: Enhancement, we can select User role and specific user (merchant or rider) to view list order assigned to him/her.

    <img src="./src/assets/demo3.png" alt="Logo" width="400">

    * Order Detail: reflect all order information

    <img src="./src/assets/demo4.png" alt="Logo" width="400">

### Main features
3. Main features:

    * Show orders management information:
        - Total orders
        - Number of orders per each order status
        - Number of orders per each processing status

    <img src="./src/assets/demo5.png" alt="Logo" width="400">

    * Filter:
        - Filter order status
        - Filter processing status
        - Filter base on updated time

    <img src="./src/assets/demo6.png" alt="Logo" width="400">

    * Search by:
        - Customer Name
        - Merchant Name
        - Rider Name
        - Order Address
        - Merchant Address
        
    <img src="./src/assets/demo7.png" alt="Logo" width="400">

    * Sort by: (click in to the column title, ex: Customer Name, ...)
        - Id
        - Customer name
        - Rider name
        - Merchant name
        - Total price
        - Updated Time (default - asc)
        - Order Address
        - Merchant Address
        - Order Status

    <img src="./src/assets/demo8.png" alt="Logo" width="400">

    * Paging:
        - Select number of orders per page
        - Go to another page

    <img src="./src/assets/demo9.png" alt="Logo" width="400">

    * Reset filter and search

    <img src="./src/assets/demo10.png" alt="Logo" width="400">

    * Reset list order: reset to list original order in the dummyData.tsx

    <img src="./src/assets/demo11.png" alt="Logo" width="400">

    * View order detail:
        - By select order in the dashboard
        - Show all order information
        - The operator can cancel an order (except status "Done" and "Canceled") by click the button "Assign to me"
        - Updated time will be set to the time operator cancel the order.

    <img src="./src/assets/demo12.png" alt="Logo" width="400">

### Extra features
4. Extra features: (Under Order Management System With User Role module)

    * Select a specific user role and user to view list order assigned to him/her.
    
    <img src="./src/assets/demo13.png" alt="Logo" width="400">

    * Only merchant can accept an order with status Created

    <img src="./src/assets/demo14.png" alt="Logo" width="400">

    * Only operator can assign an order to a specific rider if status of this order is Accepted
    
    <img src="./src/assets/demo15.png" alt="Logo" width="400">

    * Only rider can start deliver the order to the customer if status of the order is Accepted

    <img src="./src/assets/demo16.png" alt="Logo" width="400">

    * Only rider can start update status to done after the customer receive this order if status of the order is Delivering
    
    <img src="./src/assets/demo17.png" alt="Logo" width="400">

    * Auto refresh list latest order after 1 min to reflect the latest processing status (normal, late, warning)

<!-- ROADMAP -->

## Contact

Do Huynh The Dan
* [Facebook](https://www.facebook.com/chuoixanhz)
* [Gmail](mailto:dohuynhthedan@gmail.com)
* [Contact no: 0326944962]()




<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Creating React and TypeScript apps with Webpack](https://www.carlrippon.com/creating-react-and-typescript-apps-with-webpack/)
* [Readme Template](https://github.com/othneildrew/Best-README-Template)
