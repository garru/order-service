import React, { useState, Component } from "react";
import "./styles/global.css";
import { setDefaultListOrder, getListOrder } from "./shared/api";
import OrderListBaseOnUserRole from "./modules/OrderListBaseOnUserRole/OrderList";
import OrderList from "./modules/OrderList/OrderList";
import Header from "./components/Header"
import Button from "./components/Button";
import { KEY_STORE } from "./shared/constants";

setDefaultListOrder();
  class App extends Component<any, any> {

    constructor(props: any, context: any) {
      super(props, context);
      this.state = {
        withRole: false
      };
    }

  render() {
    const {withRole} = this.state;
    return(<React.Fragment>
      <Header></Header>
      <div className="nav">
        <div className={withRole ? "nav-item": "nav-item active"} onClick={() => this.setState({withRole: false})}>
          Order Management System
        </div>
        <div className={withRole ? "nav-item active": "nav-item"} onClick={() => this.setState({withRole: true})}>
          Order Management System With User Role
        </div>
        
      </div>
      <div style={{textAlign: "right", marginTop: 20}}>
      <div className="form-group">
            <Button
              btnType="button"
              text="Force Reset List Order"
              onClick={() => {
                localStorage.removeItem(KEY_STORE.LIST_ORDER);
                setDefaultListOrder();
                window.location.reload();
              }}
            ></Button>
          </div>
      </div>
      {withRole ? (
        <OrderListBaseOnUserRole></OrderListBaseOnUserRole>
      ) : (
        <OrderList></OrderList>
      )}
    </React.Fragment>)
  };
  }

export default App;
