import React from "react";

type Props = {
    btnType?: 'submit' | 'reset' | 'button';
    text: string;
    onClick?: any;
}


const Button: React.FC<Props> = ({btnType, text, onClick}) => {
  return (
      <button className="btn" type={btnType} onClick={onClick}>{text}</button>
  );
};
export default Button;
