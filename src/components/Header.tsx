import React from "react";
import Logo from "../assets/logo.png";

const Header: React.FC<any> = () => {
  return (
    <div className={"header"}>
      <a href="/"><img src={Logo} alt="Logo" /></a>
    </div>
  );
};
export default Header;
