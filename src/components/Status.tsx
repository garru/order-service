import React from "react";
import "../styles/status.css";
import { STATUS } from "../shared/constants";

type Props = {
  type: string;
  count?: string | number;
};

function getProps(type: string) {
  switch (type) {
    case STATUS.ACCEPTED:
      return {
        color: "#1B740A",
        text: "Accepted"
      };
    case STATUS.DRIVER_ASSIGNED:
      return {
        color: "#0A3B74",
        text: "Driver Assigned"
      };
    case STATUS.DELIVERING:
      return {
        color: "#E3B433",
        text: "Delivering"
      };
    case STATUS.DONE:
      return {
        color: "#33E38B",
        text: "Done"
      };
    case STATUS.CANCELED:
      return {
        color: "#5B7776",
        text: "Canceled"
      };
    default:
      return {
        color: "#05C1FA",
        text: "Created"
      };
  }
}

const Status: React.FC<Props> = ({ type, count }) => {
  const { color, text } = getProps(type);
  return count && count !== 0 ? (
    
    <div className="status-wrapper">
      <div className="status-outside">
      <span className="status" style={{ backgroundColor: color }}>
        {text}
      </span>
      </div>
      <span className="count">{count}</span>
    </div>
  ) : (
    <span className="status" style={{ backgroundColor: color }}>
      {text}
    </span>
  );
};

export default Status;
