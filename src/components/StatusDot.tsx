import React from "react";
import "../styles/status.css";
import { STATUS } from "../shared/constants";

type Props = {
  type: string | undefined;
  count?: string | number;
  hasText?: boolean
};

function getProps(type: string | undefined) {
  switch (type) {
    case STATUS.WARNING:
      return {
        color: "#FDBD41",
        text: STATUS.WARNING
      };
    case STATUS.LATE:
      return {
        color: "#D54493",
        text: STATUS.LATE
      };
    default:
      return {
        color: "#33e38b",
        text: STATUS.NORMAL
      };
  }
}

const StatusDot: React.FC<Props> = ({ type, count, hasText }) => {
  const { color, text } = getProps(type);
  return (
    <span className="status-dot">
      <span style={{ backgroundColor: color }}></span>
       &nbsp;{count} {hasText ? text : ""}
    </span>
  );
};

export default StatusDot;
