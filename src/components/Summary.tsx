import React, {useState} from "react";
import Status from "../components/Status";
import StatusDot from "../components/StatusDot";
import IconOrder from "../assets/icon_order.svg";
import {getSummary} from "../shared/api";
import { STATUS } from "../shared/constants";

const Header: React.FC<any> = () => {
  const [update, setUpdate] = useState();

  setInterval(() => {
    setUpdate(new Date());
  }, 60000);
  
  const {
    total,
    accepted,
    driverAssigned,
    delivering,
    done,
    canceled,
    created,
    normal,
    warning,
    late
  } = getSummary();
  console.log("ForceUpdate2", warning);
  return (
    <div>
      <h1>Order manager</h1>
      <div className={"summary"}>
        <div className={"summary-left"}>
          <div className="summary-left-head">
            <img src={IconOrder} alt="Total order" />
            <div className={"summary-left-content"}>
              <p>Total orders</p>
              <p style={{fontSize: 30, fontWeight: "bold"}}>{total}</p>
            </div>
          </div>
          <div className="summary-left-foot">
            <StatusDot hasText={true} type="normal" count={normal}></StatusDot>
            <StatusDot hasText={true} type="warning" count={warning}></StatusDot>
            <StatusDot hasText={true} type="late" count={late}></StatusDot>
          </div>
        </div>
        <div className={"summary-right"}>
          <div className="summary-right-wrapper">
          <div className="summary-right-item">
            <Status type={STATUS.CREATED} count={created}></Status>
          </div>
          <div className="summary-right-item">
            <Status type={STATUS.ACCEPTED} count={accepted}></Status>
          </div>
          <div className="summary-right-item">
            <Status type={STATUS.DRIVER_ASSIGNED} count={driverAssigned}></Status>
          </div>
          <div className="summary-right-item">
            <Status type={STATUS.DELIVERING} count={delivering}></Status>
          </div>
          <div className="summary-right-item">
            <Status type={STATUS.DONE} count={done}></Status>
          </div>
          <div className="summary-right-item">
            <Status type={STATUS.CANCELED} count={canceled}></Status>
          </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Header;
