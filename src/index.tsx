import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from './shared/store'
import { BrowserRouter } from 'react-router-dom';
import Routes from './shared/router';

render(
    <Provider store={store}>
        <BrowserRouter>
            <div className="fluidContainer">
                <Routes />
            </div>
        </BrowserRouter>
    </Provider>,
    document.getElementById("root")
);