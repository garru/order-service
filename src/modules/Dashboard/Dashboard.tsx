import React, { useState, Component } from "react";
import Status from "../../components/Status";
import StatusDot from "../../components/StatusDot";
import moment from "moment-mini";
import IconLeft from "../../assets/icon_arrow_left.svg";
import IconRight from "../../assets/icon_arrow_right.svg";
import { getListOrder } from "../../shared/api";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actionsOrderList from "../OrderList/action";
import * as action from "./action";
import * as actionsFilterSearch from "../FilterSearch/action";
import { Link } from "react-router-dom";
import {NUMBER_PER_PAGE, TABLE_HEADER, DEFAULT_SORT_FIELD, DEFAULT_SORT_TYPE, DEFAULT_PER_PAGE, DEFAULT_NUMBER_OF_PAGE, DATE_TIME_FORMAT} from "../../shared/constants"

const initialState = {
  option: {
    sort: {
      type: DEFAULT_SORT_FIELD,
      value: DEFAULT_SORT_TYPE
    },
    perPage: DEFAULT_PER_PAGE,
    pageNo: 1
  },
  listOrder: []
};


function generatePaging(pag: any) {
  const { pageNo, totalPage } = pag;
  let min, max;
  const hafl_number_of_page = Math.ceil(DEFAULT_NUMBER_OF_PAGE/2);
  const listPage = [];
  if (totalPage <= DEFAULT_NUMBER_OF_PAGE) {
    min = 1;
    max = totalPage;
  } else {
    if (pageNo <= hafl_number_of_page) {
      min = 1;
      max = DEFAULT_NUMBER_OF_PAGE;
    } else if (pageNo >= totalPage - hafl_number_of_page) {
      min = totalPage - DEFAULT_NUMBER_OF_PAGE;
      max = totalPage;
    } else {
      min = pageNo - hafl_number_of_page;
      max = pageNo + 2;
    }
  }
  for (let i = min; i <= max; i++) {
    listPage.push(i);
  }
  return listPage;
}

class Dashboard extends Component<any, any> {
  constructor(props: any, context: any) {
    super(props, context);
    this.state = initialState;
  }

  componentWillReceiveProps(nextProps) {
    const { option } = this.state;
    let newOption = { ...nextProps.option, ...option };
    this.getNewListOrder(newOption);
  }

  setItemPerPage(e) {
    const value = e.target.value;
    const { option } = this.state;
    const newOption = { ...this.props.option, ...option, perPage: value };
    this.setState({ option: { ...option, perPage: +value } });
    this.getNewListOrder(newOption);
  }

  onFilterSearchSort(key: string, getType: string) {
    const searchFilterOption = this.props.option;
    const option = this.state.option;
    let newOption = { ...option, ...searchFilterOption };
    switch (getType) {
      case "sort":
        const { type, value } = option.sort;
        const sort = {
          type: key,
          value: type === key && value === "asc" ? "desc" : "asc"
        };
        this.setState({ option: { ...option, sort: sort } });
        newOption.sort = sort;
        break;
    }
    this.getNewListOrder(newOption);
  }

  gotoPage(pageNo: number) {
    const option = this.state.option;
    const newOption = { ...this.props.option, ...option, pageNo: pageNo };
    this.getNewListOrder(newOption);
    this.setState({ option: { ...option, pageNo: pageNo } });
  }

  getNewListOrder(newOption) {
    const newListOrder = getListOrder(newOption);
    this.setState({ listOrder: newListOrder });
  }

  render() {
    const { option, listOrder } = this.state;
    const { data, perPage, pageNo, total } = listOrder;
    const totalPage = Math.ceil(total / perPage);
    const paging = {
      perPage: perPage,
      pageNo: pageNo,
      totalPage: totalPage
    };
    const listPage = generatePaging(paging);
    const from = total ? perPage * (pageNo - 1) + 1 : total;
    const to = total > perPage * pageNo ? perPage * pageNo : total;
    

    return (
      <div>
        <table>
          <thead>
            <tr className="thead">
              {TABLE_HEADER.map(({ text, key }) => {
                return (
                  <th
                    key={key}
                    className={`${
                      _.get(this.state, "option.sort.type") === key
                        ? "active"
                        : ""
                    } ${
                      !_.get(this.state, "option.sort.value")
                        ? ""
                        : _.get(this.state, "option.sort.value") === "desc"
                        ? "asc"
                        : "desc"
                    }`}
                    onClick={() => {
                      this.onFilterSearchSort(key, "sort");
                    }}
                  >
                    {text}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody className={"has-link"}>
            {data &&
              data.map(
                ({
                  id,
                  customerName,
                  totalPrice,
                  merchantName,
                  orderStatus,
                  processingStatus,
                  updatedTime,
                  orderAddress,
                  merchantAddress,
                  riderName
                }) => {
                  return (
                    <tr key={id}>
                      <td>
                        <Link to={`/order/${id}`}>
                          <StatusDot
                            type={processingStatus}
                            hasText={false}
                            count={id}
                          ></StatusDot>
                        </Link>
                      </td>
                      <td>
                        <Link to={`/order/${id}`}>{customerName}</Link>
                      </td>
                      <td>
                        <Link to={`/order/${id}`}>
                          {Number(totalPrice.toFixed(1)).toLocaleString()}
                        </Link>
                      </td>
                      <td>
                        <Link to={`/order/${id}`}>{merchantName}</Link>
                      </td>
                      <td>
                        <Link to={`/order/${id}`}>{riderName}</Link>
                      </td>
                      <td>
                        <Link to={`/order/${id}`}>
                          <Status type={orderStatus} />
                        </Link>
                      </td>
                      <td>
                        <Link to={`/order/${id}`}>
                          {moment(updatedTime).format(DATE_TIME_FORMAT)}
                        </Link>
                      </td>
                      <td>
                        <Link to={`/order/${id}`}>{orderAddress}</Link>
                      </td>
                      <td>
                        <Link to={`/order/${id}`}>{merchantAddress}</Link>
                      </td>
                    </tr>
                  );
                }
              )}
          </tbody>
        </table>
        {total === 0 && <div className={"no-order"}>No orders found</div>}
        <div className="paging-wrapper">
          <div className="form-group">
            <span>Number per page </span>
            <select
              name="perPage"
              onChange={e => {
                this.setItemPerPage(e);
              }}
              style={{ width: 50, padding: "5px", height: 30 }}
              defaultValue={option.perPage}
            >
              {NUMBER_PER_PAGE.map(value => {
                return (
                  <option key={value} value={value}>
                    {value}
                  </option>
                );
              })}
            </select>
          </div>
          <span style={{ marginRight: 20 }}>
            Showing orders from {from} to {to} of {total} in {totalPage} pages
          </span>
          <div className="paging">
            <ul>
              <li
                key={"First"}
                className={pageNo <= 1 ? "disabled" : ""}
                onClick={() => this.gotoPage(1)}
              >
                First
              </li>
              <li
                key={"Prev"}
                className={pageNo <= 1 ? "disabled" : ""}
                onClick={() => this.gotoPage(pageNo - 1)}
              >
                <img src={IconLeft} alt="Prev" />
              </li>
              {listPage.map(index => (
                <li
                  key={index}
                  className={pageNo === index ? "active" : ""}
                  onClick={() => this.gotoPage(index)}
                >
                  {index}
                </li>
              ))}
              <li
                key={"Next"}
                className={pageNo >= totalPage ? "disabled" : ""}
                onClick={() => this.gotoPage(pageNo + 1)}
              >
                <img src={IconRight} alt="Next" />
              </li>
              <li
                key={"Last"}
                className={pageNo >= totalPage ? "disabled" : ""}
                onClick={() => this.gotoPage(totalPage)}
              >
                Last
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

//  Set the actions which will prompt the reducers to check for matching types
function mapDispatchToProps(dispatch: any) {
  return bindActionCreators(
    {
      loadListOrder: actionsOrderList.loadListOrder,
      updateSort: actionsFilterSearch.updateSort
    },
    dispatch
  );
}

function mapStateToProps(state: any) {
  return {
    listOrder: state.OrderListReducer.listOrder,
    option: state.FilterReducer
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
