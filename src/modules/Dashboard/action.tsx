import { FILTER_SORT_SEARCH } from './actionType'
import { GetOrder } from '../../shared/modal';

export const filterSortSearch = (option: GetOrder) =>({
    type: FILTER_SORT_SEARCH,
    data: option
})

