import {
  FILTER_SORT_SEARCH
} from "./actionType";
export default function DashboardReducer(
  state = {
    option: {}
  },
  action: any
) {
  switch (action.type) {
    case FILTER_SORT_SEARCH: {
      return Object.assign({}, state, {
        option: { ...state.option, ...action.data }
      });
    }
    default:
      return state;
  }
}
