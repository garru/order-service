import React, { useState, Component } from "react";
import IconSearch from "../../assets/icon_search.svg";
import {
  ORDER_STATUS,
  PROCESSING_STATUS,
  LAST_UPDATE,
  USER_ROLE,
  OPERATOR_NAME,
  KEY_STORE,
  ROLE
} from "../../shared/constants";
import Button from "../../components/Button";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "./action";
import * as DashboardActions from "../Dashboard/action";
import { FilterSort } from "../../shared/modal";
import * as api from "../../shared/api";

type IState = {
  search: string;
  filter: FilterSort[];
  orderStatus: string;
  processingStatus: string;
  updatedTime: string;
  role: string;
  userName: string;
  listUser: any[];
};

let timer;

type IFilter =
  | "orderStatus"
  | "processingStatus"
  | "updatedTime"
  | "merchantName"
  | "riderName";

const initialstate = {
  search: "",
  filter: [],
  orderStatus: "",
  processingStatus: "",
  updatedTime: "",
  role: "operator",
  userName: "",
  listUser: []
};

class FilterSearch extends Component<any, IState> {
  constructor(props: any, context: any) {
    super(props, context);
    this.state = initialstate;
    this.props.updateSearch("");
    this.props.updateFilter([]);
    setInterval(() => {
      this.props.refresh()
    }, 60000)
  }

  handleChangeSearch(e: any) {
    const value = e.target.value;
    this.setState({ search: value });
    clearTimeout(timer);
    timer = setTimeout(() => {
      this.props.updateSearch(value);
    }, 250);
  }

  handleChangeFilter(type: IFilter, e) {
    const value = e.target.value;

    const newFilterOption = [];
    switch (type) {
      case "orderStatus":
        this.setState({ orderStatus: value });
        break;
      case "processingStatus":
        this.setState({ processingStatus: value });
        break;

      case "merchantName":
        this.setState({ userName: value });
        break;
      case "riderName":
        this.setState({ userName: value });
        break;
      default:
        this.setState({ updatedTime: value });
        break;
    }

    setTimeout(() => {
      const {
        orderStatus,
        processingStatus,
        updatedTime,
        role,
        userName
      } = this.state;
      orderStatus &&
        newFilterOption.push({ type: "orderStatus", value: orderStatus });
      processingStatus &&
        newFilterOption.push({
          type: "processingStatus",
          value: processingStatus
        });
      updatedTime &&
        newFilterOption.push({ type: "updatedTime", value: updatedTime });
      role &&
        userName &&
        newFilterOption.push({
          type: role === ROLE.MERCHANT ? "merchantName" : "riderName",
          value: userName
        });
      this.props.updateFilter(newFilterOption);
      this.props.filterSortSearch(newFilterOption);
    }, 200);
  }

  resetAll() {
    this.setState(initialstate);
    this.props.updateSearch("");
    this.props.updateFilter([]);
  }

  handleChangeUserRole(e) {
    const value = e.target.value;
    this.setState({ role: value });
    const listUser = api.getAllUser(value);
    this.setState({ listUser: listUser });
    localStorage.setItem(KEY_STORE.ROLE, value);
    localStorage.setItem(KEY_STORE.USER_INFO, OPERATOR_NAME);
  }

  handleChangeUser(e) {
    const type = this.state.role === ROLE.MERCHANT ? "merchantName" : "riderName";
    this.handleChangeFilter(type, e);
    api.setUserInfo(e.target.value);
  }

  render() {
    const {
      orderStatus,
      processingStatus,
      updatedTime,
      search,
      role,
      userName,
      listUser
    } = this.state;
    const { isBaseOnRole } = this.props;
    return (
      <div>
        {isBaseOnRole && (
          <div className="form-filter" style={{ marginBottom: 30 }}>
            <div className="form-group">
              <select
                name="role"
                placeholder="All order status"
                onChange={e => {
                  this.handleChangeUserRole(e);
                }}
                value={role}
              >
                {USER_ROLE.map(opt => {
                  return (
                    <option key={opt.role} value={opt.role}>
                      {opt.text}
                    </option>
                  );
                })}
              </select>
            </div>
            {listUser.length ? (
              <div className="form-group">
                <select
                  name="role"
                  placeholder="All user"
                  onChange={e => {
                    this.handleChangeUser(e);
                  }}
                  value={userName}
                >
                  <option value="">Select user</option>
                  {listUser.map(name => {
                    if(name) {
                      return (
                        <option key={name} value={name}>
                          {name}
                        </option>
                      );
                    } else {
                      return;
                    }
                  })}
                </select>
              </div>
            ) : (
              <div className="form-group"></div>
            )}
            <div className="form-group"></div>
            <div className="form-group"></div>
            <div className="form-group"></div>
          </div>
        )}

        <div className="form-filter">
          <div className="form-group">
            <input
              name="search"
              type="text"
              placeholder="Type search here"
              onChange={e => this.handleChangeSearch(e)}
              defaultValue={search}
            />
            <img className="form-group-search" src={IconSearch} alt="Search" />
          </div>

          <div className="form-group">
            <select
              name="orderStatus"
              placeholder="All order status"
              onChange={e => {
                this.handleChangeFilter("orderStatus", e);
              }}
              value={orderStatus}
            >
              {ORDER_STATUS.map(opt => {
                return (
                  <option key={opt.code} value={opt.code}>
                    {opt.text}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="form-group">
            <select
              name="processingStatus"
              value={processingStatus}
              onChange={e => {
                this.handleChangeFilter("processingStatus", e);
              }}
            >
              {PROCESSING_STATUS.map(opt => {
                return (
                  <option key={opt.code} value={opt.code}>
                    {opt.text}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="form-group">
            <select
              name="updatedTime"
              onChange={e => {
                this.handleChangeFilter("updatedTime", e);
              }}
              value={updatedTime}
            >
              {LAST_UPDATE.map(opt => {
                return (
                  <option key={opt.code} value={opt.code}>
                    {opt.text}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="form-group">
            <Button
              btnType="button"
              text="Reset"
              onClick={() => this.resetAll()}
            ></Button>
          </div>
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch: any) {
  return bindActionCreators(
    {
      updateSearch: actions.updateSearch,
      updateFilter: actions.updateFilter,
      filterSortSearch: DashboardActions.filterSortSearch,
      refresh:actions.refresh
    },
    dispatch
  );
}

export default connect(
  null,
  mapDispatchToProps
)(FilterSearch);
