import { UPDATE_FILTER, UPDATE_SEARCH, UPDATE_SORT, RESET,LIVE_LOAD } from "./actionType";

export function updateSearch(search) {
    return { type: UPDATE_SEARCH, data: search };
}
export function updateSort(sort) {
    return { type: UPDATE_SORT, data: sort };
}
export function updateFilter(filter) {
    return { type: UPDATE_FILTER, data: filter };
}
export function reset(filter) {
    return { type: RESET, data: filter };
}
export const refresh = (id: number) =>({
    type: LIVE_LOAD,
    data: id
})
