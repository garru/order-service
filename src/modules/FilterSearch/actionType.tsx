export const UPDATE_FILTER = "UPDATE_FILTER";
export const UPDATE_SORT = "UPDATE_SORT";
export const UPDATE_SEARCH = "UPDATE_SEARCH";
export const RESET = "RESET";
export const LIVE_LOAD = "LIVE_LOAD";