import React, { Component } from "react";
import "../../styles/global.css";
import * as api from "../../shared/api";
import { connect } from "react-redux";
import StatusDot from "../../components/StatusDot";
import Status from "../../components/Status";
import Button from "../../components/Button";
import moment from "moment-mini";
import { ROLE, STATUS, DATE_TIME_FORMAT } from "../../shared/constants";

class OrderItem extends Component<any, any> {
  constructor(props, context) {
    super(props, context);
    const id = +this.props.match.params.id;
    const currentOrder = api.getOrderById(id);
    this.state = { order: currentOrder };
  }

  handleChangeDrive(e, id, orderStatus) {
    this.setState({ riderName: e.target.value });
    this.updateOrderStatus(id, orderStatus,e.target.value);
  }

  updateOrderStatus(id, orderStatus, riderName?) {
    const userName = api.getUserInfo();
    let orderUpdated = {
      id: id,
      orderStatus: orderStatus,
      updatedTime: moment().format(DATE_TIME_FORMAT)
    };
    if (orderStatus === STATUS.DRIVER_ASSIGNED) {
      orderUpdated["assignedTo"] = riderName;
      orderUpdated["riderName"] = riderName;
    } else {
      orderUpdated["assignedTo"] = userName;
    }

    api.updateOrderById(orderUpdated);
    const newOrder = api.getOrderById(id);
    this.setState({ order: newOrder });
  }

  renderButtons(id, role, orderStatus) {
    const listUser = api.getAllUser(ROLE.RIDER);
    switch (role) {
      case ROLE.MERCHANT:
        return (
          <React.Fragment>
            {orderStatus === STATUS.CREATED && (
              <Button
                onClick={() => this.updateOrderStatus(id, STATUS.ACCEPTED)}
                btnType="button"
                text="Accept Order"
              ></Button>
            )}
          </React.Fragment>
        );
      case ROLE.RIDER:
        return (
          <React.Fragment>
            {orderStatus === STATUS.DRIVER_ASSIGNED && (
              <Button
                onClick={() => this.updateOrderStatus(id, STATUS.DELIVERING)}
                btnType="button"
                text="Start delivery"
              ></Button>
            )}
            {orderStatus === STATUS.DELIVERING && (
              <Button
                onClick={() => this.updateOrderStatus(id, STATUS.DONE)}
                btnType="button"
                text="Customer received"
              ></Button>
            )}
          </React.Fragment>
        );
      default:
        return (
          <React.Fragment>
            {orderStatus !== STATUS.DONE && orderStatus !== STATUS.CANCELED && (
              <Button
                btnType="button"
                text="Assign to me"
                onClick={() => this.updateOrderStatus(id, STATUS.CANCELED)}
              ></Button>
            )}

            {orderStatus === STATUS.ACCEPTED && (
              <div className="order-detail-item">
                <div className="order-detail-item-left">Assign to driver</div>
                <div className="order-detail-item-right">
                  <div className="form-group">
                    <select
                      name="role"
                      placeholder="Select driver"
                      onChange={e => {
                        this.handleChangeDrive(e, id, STATUS.DRIVER_ASSIGNED);
                      }}
                      value={this.state.riderName}
                    >
                      <option value="">Select driver to assign</option>
                      {listUser.map(name => {
                        return (
                          <option key={name} value={name}>
                            {name}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                </div>
              </div>
            )}
          </React.Fragment>
        );
    }
  }

  render() {
    const {
      id,
      customerName,
      merchantAddress,
      orderAddress,
      merchantName,
      riderName,
      orderStatus,
      dishes,
      processingStatus,
      totalPrice,
      updatedTime,
      assignedTo
    } = this.state.order;
    const userRole = api.getUserRole();

    return (
      <div className="wrapper">
        <h1 style={{ marginBottom: 20 }}>Order Detail</h1>
        <a style={{ marginBottom: 20, display: "block" }} href="/">
          {" "}
          &lt;&lt;&lt; Back to list orders
        </a>
        <div className="form-group" style={{ marginBottom: 30 }}>
          {this.renderButtons(id, userRole, orderStatus)}
        </div>
        <div className="order-detail">
          <div className="order-detail-item">
            <div className="order-detail-item-left">ID</div>
            <div className="order-detail-item-right">{id}</div>
          </div>
          <div className="order-detail-item">
            <div className="order-detail-item-left">Customer Name</div>
            <div className="order-detail-item-right">{customerName}</div>
          </div>
          <div className="order-detail-item">
            <div className="order-detail-item-left">Total Price</div>
            <div className="order-detail-item-right">
              <b>{Number(totalPrice.toFixed(1)).toLocaleString()}</b>
            </div>
          </div>
          <div className="order-detail-item">
            <div className="order-detail-item-left">Processing Status</div>
            <div className="order-detail-item-right">
              <StatusDot type={processingStatus} hasText={true}></StatusDot>
            </div>
          </div>
          <div className="order-detail-item">
            <div className="order-detail-item-left">Order Address</div>
            <div className="order-detail-item-right">{orderAddress}</div>
          </div>
          <div className="order-detail-item">
            <div className="order-detail-item-left">Merchant Address</div>
            <div className="order-detail-item-right">{merchantAddress}</div>
          </div>
          <div className="order-detail-item">
            <div className="order-detail-item-left">Merchant Name</div>
            <div className="order-detail-item-right">{merchantName}</div>
          </div>

          <div className="order-detail-item">
            <div className="order-detail-item-left">Rider Name</div>
            <div className="order-detail-item-right">{riderName}</div>
          </div>

          <div className="order-detail-item">
            <div className="order-detail-item-left">Order Status</div>
            <div className="order-detail-item-right">
              <Status type={orderStatus}></Status>
            </div>
          </div>

          <div className="order-detail-item">
            <div className="order-detail-item-left">Updated Time</div>
            <div className="order-detail-item-right">{updatedTime}</div>
          </div>
          <div className="order-detail-item">
            <div className="order-detail-item-left">Assigned To</div>
            <div className="order-detail-item-right">{assignedTo}</div>
          </div>
        </div>
        <h3>Dishes</h3>
        <table>
          <thead>
            <tr className="thead">
              <th>No.</th>
              <th>Name</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {dishes.map(({ name, price }, idx) => (
              <tr key ={idx}>
                <td>{idx}</td>
                <td>{name}</td>
                <td>{price}</td>
              </tr>
            ))}
            <tr></tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default connect(
  null
)(OrderItem);
