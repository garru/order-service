import React, { Component } from "react";
import "../../styles/global.css";
import Summary from "../../components/Summary";
import FilterSearch from "../FilterSearch/FilterSearch";
import * as api from "../../shared/api";
import Dashboard from "../Dashboard/Dashboard";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './action';
import { KEY_STORE } from "../../shared/constants";

class OrderList extends Component<any> {

  componentDidMount() {
    const allOrders = api.getAllOrder();
    this.props.loadListOrder(allOrders);
    localStorage.removeItem(KEY_STORE.ROLE);
    localStorage.removeItem(KEY_STORE.USER_INFO);
}

  render() {
    return (
      <div>
        <div className="wrapper">
          <div style={{ marginTop: 40 }}>
            <Summary></Summary>
          </div>

          <div style={{ marginTop: 40 }}>
            <FilterSearch></FilterSearch>
          </div>
          <div style={{ marginTop: 40 }}>
            <Dashboard></Dashboard>
          </div>
        </div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
      loadListOrder: actions.loadListOrder
  }, dispatch);
}

export default connect(null, mapDispatchToProps)(OrderList);
