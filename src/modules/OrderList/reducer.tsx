import { LOAD_LIST_ORDER } from "./actionType";
export default function OrderListReducer(
  state = {
    listOrder: {},
    option: {}
  },
  action: any
) {
  switch (action.type) {
    case LOAD_LIST_ORDER: {
      return Object.assign({}, state, { listOrder: action.data });
    }
    default:
      return state;
  }
}
