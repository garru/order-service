import { LOAD_LIST_ORDER } from "./actionType";

export function loadListOrder(response) {
    return { type: LOAD_LIST_ORDER, data: response };
}
