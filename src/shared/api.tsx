import { LIST_ORDER } from "./dummyData";
import { Order, GetOrder, FilterSort, ResponseListOrder } from "../shared/modal";
import moment from "moment-mini";
import * as _ from "lodash";
import { KEY_STORE, ROLE, STATUS, DATE_TIME_FORMAT, DEFAULT_SORT_FIELD, DEFAULT_SORT_TYPE, PROCESSING_TIME } from "./constants";

const DEFAULT_OPTION: GetOrder = {
  perPage: 10,
  pageNo: 1,
  search: "",
  filter: [],
  sort: {
    type: DEFAULT_SORT_FIELD,
    value: DEFAULT_SORT_TYPE
  },
};

function getListOrderFromLocalStorage() {
  const currentData = localStorage.getItem(KEY_STORE.LIST_ORDER);
  return currentData ? JSON.parse(currentData) : null;
}

export function setListOrder(listOrder: string) {
  localStorage.setItem(KEY_STORE.LIST_ORDER, listOrder);
}

export function setDefaultListOrder() {
  const currentData = getListOrderFromLocalStorage();
  !currentData && setListOrder(JSON.stringify(LIST_ORDER));
}

export function updateOrderById(order: any) {
  const { id } = order;
  const listOrder = getListOrderFromLocalStorage();
  const currentOrderIndex = listOrder.findIndex((ord: Order) => ord.id === id);
  const newOrder = Object.assign(listOrder[currentOrderIndex], order);
  listOrder[currentOrderIndex] = newOrder;
  setListOrder(JSON.stringify(listOrder));
}

export function getOrderById(id: number) {
  const listOrder = getListOrderFromLocalStorage();
  const order = listOrder.find((ord: Order) => ord.id === id);
  return order;
}

export function searchOrders(data: Order[], searchText: string) {
  // search by Customer Name, Merchant Name, Rider Name, Order Address, Merchant Address
  let dataAfterSearch;
  const newSearchText = searchText.toLocaleLowerCase().trim();
  if (searchText) {
    dataAfterSearch = data.filter(order => {
      const {
        customerName,
        merchantAddress,
        merchantName,
        riderName,
        orderAddress
      } = order;
      return (
        customerName.toLocaleLowerCase().indexOf(newSearchText) !== -1 ||
        merchantAddress.toLocaleLowerCase().indexOf(newSearchText) !== -1 ||
        merchantName.toLocaleLowerCase().indexOf(newSearchText) !== -1 ||
        riderName.toLocaleLowerCase().indexOf(newSearchText) !== -1 ||
        orderAddress.toLocaleLowerCase().indexOf(newSearchText) !== -1
      );
    });
  } else {
    dataAfterSearch = data;
  }

  return dataAfterSearch;
}

export function filerSortOrders(
  data: Order[],
  filter: FilterSort[],
  sort: FilterSort
) {
  let listOrder: Order[] = data.map((order: Order, idx: number) => {
    const processingStatus = getProcessStatus(
      order.updatedTime,
      order.orderStatus
    );
    return {...order, processingStatus: processingStatus};
  });

  if (filter&&filter.length) {
    filter.forEach(fil => {
        listOrder = listOrder.filter(
          (order: any) => {
            if (fil.type === "updatedTime") {
              const  now = moment();
              const updatedTime = moment(order.updatedTime);
              const diff = now.diff(updatedTime, "minute");
              console.log(updatedTime, diff);
              return diff <= +fil.value;
            } else {
              return order[fil.type] === fil.value}
            }
        );
    })
  } else {
    listOrder = listOrder;
  }
  listOrder = listOrder.sort((a: any, b: any) => {
    let sortValueA, sortValueB;
    if (sort.type) {
      if (typeof a[sort.type] === "number") {
        sortValueA = a[sort.type];
        sortValueB = b[sort.type];
      } else {
        sortValueA = a[sort.type].toLowerCase();
        sortValueB = b[sort.type].toLowerCase();
      }
    } else {
      sortValueA = moment(a.updatedTime, DATE_TIME_FORMAT).valueOf();
      sortValueB = moment(b.updatedTime, DATE_TIME_FORMAT).valueOf();
    }
    sort.value = sort.value || DEFAULT_SORT_TYPE;
    if ((sort.value === "asc")) {
      if (sortValueA < sortValueB)
        //sort string ascending
        return -1;
      if (sortValueA > sortValueB) return 1;
      return 0;
    } else {
      if (sortValueA < sortValueB)
        //sort string descending
        return 1;
      if (sortValueA > sortValueB) return -1;
      return 0;
    }
  });
  

  return listOrder;
}

export function getListOrder(option: GetOrder):ResponseListOrder {
  const { perPage, pageNo, search, filter, sort } = option;
  let newPageNo: number, totalPage: number;
  let orderAfterSearch, orderAfterFilterSort;
  const allOrders = getListOrderFromLocalStorage();
  orderAfterSearch = searchOrders(allOrders, search);
  orderAfterFilterSort = filerSortOrders(orderAfterSearch, filter, sort);
  const total = orderAfterFilterSort.length;
  totalPage = Math.ceil(total/(+perPage));
  newPageNo = pageNo > totalPage ? totalPage : pageNo;
  const from = newPageNo ? (+perPage) * (newPageNo - 1) : 0;
  const listOrder = orderAfterFilterSort.splice(from, (+perPage));
  return {
    data: listOrder,
    pageNo: newPageNo,
    perPage: perPage,
    total: total
  };
}

export function getSummary() {
  const currentData = getListOrderFromLocalStorage();
  let total = currentData.length;
  let accepted = 0;
  let driverAssigned = 0;
  let delivering = 0;
  let done = 0;
  let canceled = 0;
  let created = 0;
  let normal = 0;
  let warning = 0;
  let late = 0;

  currentData.forEach((order: Order) => {
    switch (order.orderStatus) {
      case STATUS.ACCEPTED:
        accepted++;
        break;
      case STATUS.DRIVER_ASSIGNED:
        driverAssigned++;
        break;
      case STATUS.DELIVERING:
        delivering++;
        break;
      case STATUS.DONE:
        done++;
        break;
      case STATUS.CANCELED:
        canceled++;
        break;
      default:
        created++;
        break;
    }
    let processStatus = getProcessStatus(order.updatedTime, order.orderStatus);
    switch (processStatus) {
      case STATUS.LATE:
        late++;
        break;
      case STATUS.WARNING:
        warning++;
        break;
      default:
        normal++;
        break;
    }
  });
  return {
    total: total,
    created: created,
    accepted: accepted,
    driverAssigned: driverAssigned,
    delivering: delivering,
    done: done,
    canceled: canceled,
    normal: normal,
    warning: warning,
    late: late
  };
}

function getProcessStatus(updatedTime: string, orderStatus: string) {
  const now = moment();
  const diff = now.diff(updatedTime, "minute");
  if(orderStatus === STATUS.DONE || orderStatus === STATUS.CANCELED) return STATUS.NORMAL;
  if (orderStatus === STATUS.DELIVERING) {
    if (diff >= PROCESSING_TIME.LATE_DELIVERY) return STATUS.LATE;
    if (diff >= PROCESSING_TIME.WARNING_DELIVERY) return STATUS.WARNING;
    return STATUS.NORMAL;
  } else {
    if (diff >= PROCESSING_TIME.LATE) return STATUS.LATE;
    if (diff >= PROCESSING_TIME.WARNING) return STATUS.WARNING;
    return STATUS.NORMAL;
  }
}

export function getAllOrder() {
  return getListOrder(DEFAULT_OPTION);
}

export function getAllUser(role: any): any[] {
  const allOrders = getListOrderFromLocalStorage();
  const listUser = allOrders.map(order => 
    {
      if (role === ROLE.MERCHANT) return order.merchantName;
      else return order.riderName;
      }
    )
  const listUserAfterFilter = _.uniq(listUser).filter(item => item !== "");
    return listUserAfterFilter;
}

export function getUserRole() {
  const role = localStorage.getItem(KEY_STORE.ROLE);
  return role || ROLE.OPERATOR
}

export function setUserInfo(name) {
  localStorage.setItem(KEY_STORE.USER_INFO, name);
}

export function getUserInfo() {
  return localStorage.getItem(KEY_STORE.USER_INFO);
}