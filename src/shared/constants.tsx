export const ORDER_STATUS = [
  {
    text: "All order status",
    code: ""
  },
  {
    text: "Created",
    code: "created"
  },
  {
    text: "Accepted",
    code: "accepted"
  },
  {
    text: "Driver Assigned",
    code: "driverAssigned"
  },
  {
    text: "Delivering",
    code: "delivering"
  },
  {
    text: "Done",
    code: "done"
  },
  {
    text: "Canceled",
    code: "canceled"
  }
];

export const PROCESSING_STATUS = [
  {
    text: "All processing status",
    code: ""
  },
  {
    text: "normal",
    code: "normal"
  },
  {
    text: "warning",
    code: "warning"
  },
  {
    text: "late",
    code: "late"
  }
];
export const LAST_UPDATE = [
  {
    text: "All updated time",
    code: ""
  },
  {
    text: "Last 5 mins",
    code: "5"
  },
  {
    text: "Last 10 mins",
    code: "10"
  },
  {
    text: "Last 15 mins",
    code: "15"
  }
];

export const USER_ROLE = [
  {
    text: "Operator",
    role: "operator"
  },
  {
    text: "Merchant",
    role: "merchant"
  },
  {
    text: "Rider",
    role: "rider"
  }
];

export const NUMBER_PER_PAGE = ["5", "10", "15", "20"];

export const TABLE_HEADER = [
  {
    text: "ID",
    key: "id"
  },
  {
    text: "Customer Name",
    key: "customerName"
  },
  {
    text: "Total Price",
    key: "totalPrice"
  },
  {
    text: "Merchant Name",
    key: "merchantName"
  },
  {
    text: "Rider Name",
    key: "riderName"
  },
  {
    text: "Order Status",
    key: "orderStatus"
  },
  {
    text: "Updated Time",
    key: "updatedTime"
  },
  {
    text: "Order Address",
    key: "orderAddress"
  },
  {
    text: "Merchant Address",
    key: "merchantAddress"
  }
];

export const OPERATOR_NAME = "Do Huynh The Dan";

export const KEY_STORE = {
  USER_INFO: "USER_INFO",
  ROLE: "ROLE",
  LIST_ORDER: "LIST_ORDER"
};


export const ROLE = {
  MERCHANT: "merchant",
  OPERATOR: "operator",
  RIDER: "rider"
}

export const STATUS = {
  CREATED: "created",
  ACCEPTED: "accepted",
  DRIVER_ASSIGNED: "driverAssigned",
  DELIVERING: "delivering",
  DONE: "done",
  CANCELED: "canceled",
  LATE: "late",
  WARNING: "warning",
  NORMAL: "normal"
}


export const DATE_TIME_FORMAT = "YYYY-MM-DD HH:mm:ss"

export const DEFAULT_SORT_FIELD = "updatedTime";

export const DEFAULT_SORT_TYPE = "asc";

export const DEFAULT_PER_PAGE = 10;

export const DEFAULT_NUMBER_OF_PAGE = 5;

export const PROCESSING_TIME = {
  LATE: 15,
  WARNING: 10,
  LATE_DELIVERY: 45,
  WARNING_DELIVERY: 30
}