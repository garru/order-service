export type Dish = {
  name: string;
  price: number;
};

export type FilterSort = {
    type: string;
    value: string | number;
  };

export type Order = {
  id: number;
  customerName: string;
  riderName: string;
  orderStatus: string;
  orderAddress: string;
  merchantName: string;
  merchantAddress: string;
  dishes: Dish[];
  totalPrice: number;
  updatedTime: string;
  processingStatus?: string;
  assignedTo?: string;
};

export type GetOrder = {
    perPage: number;
    pageNo: number;
    search: string;
    filter: FilterSort[];
    sort: FilterSort;
}

export type Summary = {
    total: number;
    created: number;
    accepted: number;
    driverAssigned: number;
    delivering: number;
    done: number;
    canceled: number;
    normal: number;
    warning: number;
    late: number;
  };


  export type ResponseListOrder = {
    perPage: number;
    pageNo: number;
    total: number;
    data: Order[];
  }