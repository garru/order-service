  
import React from 'react';
import { Switch, Route } from 'react-router-dom'
import App from '../App';
import OrderItem from '../modules/OrderItem/OrderItem';

const Routes = () => (
    <div>
        <main>
            <Switch>
                <Route exact path="/" component={App} />
                <Route exact path="/order/:id" component={OrderItem} />
            </Switch>
        </main>
    </div>
);

export default Routes;