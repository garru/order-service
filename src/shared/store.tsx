import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import { createLogger } from 'redux-logger';
import DashboardReducer from '../modules/Dashboard/reducer';
import FilterReducer from '../modules/FilterSearch/reducer';
import OrderListReducer from '../modules/OrderList/reducer';
import OrderListBaseOnUserRoleReducer from '../modules/OrderListBaseOnUserRole/reducer';

const rootReducer = combineReducers({
    OrderListReducer,
    DashboardReducer,
    FilterReducer
});

const initialState = {};
const enhancers = [];
const middleware = [
    createLogger({ collapsed: true })
];

const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
)

const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers
)

export default store;